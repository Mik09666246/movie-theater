package com.jpmc.theater.utils;

import com.jpmc.theater.model.Movie;
import com.jpmc.theater.model.Showing;
import lombok.RequiredArgsConstructor;

import java.time.LocalTime;
import java.util.Map;

@RequiredArgsConstructor
public class TicketPriceCalculator {
    private final int movieCodeSpecial;
    private final Map<Integer, Integer> sequenceDiscounts;
    private final LocalTime midDayStart;
    private final LocalTime midDayEnd;

    public double calculateTicketPrice(Showing showing) {
        Movie movie = showing.getMovie();
        return movie.getBasePrice() - getDiscount(showing, movie);
    }

    private double getDiscount(Showing showing, Movie movie) {
        double basePrice = movie.getBasePrice();
        int code = movie.getCode();
        double specialDiscount = 0;
        if (movieCodeSpecial == code) {
            specialDiscount = basePrice * 0.2;  // 20% discount for special movie
        }

        int showSequence = showing.getSequenceOfTheDay();
        double sequenceDiscount = sequenceDiscounts.getOrDefault(showSequence, 0);

        double midDayDiscount = 0;
        LocalTime startTime = showing.getStartTime().toLocalTime();
        if (!startTime.isBefore(midDayStart) && !startTime.isAfter(midDayEnd)) {
            midDayDiscount = basePrice * 0.25;
        }

        // biggest discount wins
        return Math.max(Math.max(specialDiscount, sequenceDiscount), midDayDiscount);
    }
}
