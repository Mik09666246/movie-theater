package com.jpmc.theater.utils;

import java.time.LocalDate;

@FunctionalInterface
public interface LocalDateProvider {
    LocalDate currentDate();

    static LocalDate present() {
        return LocalDate.now();
    }
}
