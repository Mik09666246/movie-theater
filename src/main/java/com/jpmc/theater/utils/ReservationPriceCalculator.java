package com.jpmc.theater.utils;

import com.jpmc.theater.model.Reservation;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ReservationPriceCalculator {
    private final TicketPriceCalculator ticketPriceCalculator;

    public double calculateReservationPrice(Reservation reservation) {
        return ticketPriceCalculator.calculateTicketPrice(reservation.getShowing()) * reservation.getAudienceCount();
    }
}
