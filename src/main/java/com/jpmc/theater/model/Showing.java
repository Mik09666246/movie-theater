package com.jpmc.theater.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Getter
@RequiredArgsConstructor
public class Showing {
    private final Movie movie;
    private final int sequenceOfTheDay;
    private final LocalDateTime startTime;
}
