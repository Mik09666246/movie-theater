package com.jpmc.theater.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Duration;

@EqualsAndHashCode
@Getter
@RequiredArgsConstructor
public class Movie {
    private final String title;
    private final Duration runningTime;
    private final double basePrice;
    private final int code;
    private String description;
}