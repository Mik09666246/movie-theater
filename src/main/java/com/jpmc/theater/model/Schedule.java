package com.jpmc.theater.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Schedule {
    private final int sequenceOfTheDay;
    private final String startTime;
    private final String title;
    private final String runningTime;
    private final String ticketPrice;
}
