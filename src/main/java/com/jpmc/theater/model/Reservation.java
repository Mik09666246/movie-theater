package com.jpmc.theater.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Reservation {
    private final Customer customer;
    private final Showing showing;
    private final int audienceCount;
}