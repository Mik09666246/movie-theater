package com.jpmc.theater;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jpmc.theater.model.*;
import com.jpmc.theater.utils.LocalDateProvider;
import com.jpmc.theater.utils.TicketPriceCalculator;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Theater {
    private static final String TABLE_BORDER = "===================================================";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private LocalDateProvider provider;
    private TicketPriceCalculator ticketPriceCalculator;
    private List<Showing> schedule;

    public Theater(LocalDateProvider provider, TicketPriceCalculator ticketPriceCalculator) {
        this.provider = provider;
        this.ticketPriceCalculator = ticketPriceCalculator;

        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1);
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
        Movie theBatMan = new Movie("The Batman", Duration.ofMinutes(95), 9, 0);
        LocalDate currentDate = provider.currentDate();
        schedule = List.of(
            new Showing(turningRed, 1, LocalDateTime.of(currentDate, LocalTime.of(9, 0))),
            new Showing(spiderMan, 2, LocalDateTime.of(currentDate, LocalTime.of(11, 0))),
            new Showing(theBatMan, 3, LocalDateTime.of(currentDate, LocalTime.of(12, 50))),
            new Showing(turningRed, 4, LocalDateTime.of(currentDate, LocalTime.of(14, 30))),
            new Showing(spiderMan, 5, LocalDateTime.of(currentDate, LocalTime.of(16, 10))),
            new Showing(theBatMan, 6, LocalDateTime.of(currentDate, LocalTime.of(17, 50))),
            new Showing(turningRed, 7, LocalDateTime.of(currentDate, LocalTime.of(19, 30))),
            new Showing(spiderMan, 8, LocalDateTime.of(currentDate, LocalTime.of(21, 10))),
            new Showing(theBatMan, 9, LocalDateTime.of(currentDate, LocalTime.of(23, 0)))
        );
    }

    public Reservation reserve(Customer customer, int sequence, int howManyTickets) {
        Showing showing;
        try {
            showing = schedule.get(sequence - 1);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            throw new IllegalStateException("not able to find any showing for given sequence " + sequence);
        }
        return new Reservation(customer, showing, howManyTickets);
    }

    public void printSchedule() {
        System.out.println(provider.currentDate());
        System.out.println(TABLE_BORDER);
        schedule.forEach(s -> {
            String runningTimeFormatted = humanReadableFormat(s.getMovie().getRunningTime());
            String showingFormatted = String.format("%d: %s %s %s $%s",
                s.getSequenceOfTheDay(),
                s.getStartTime(),
                s.getMovie().getTitle(),
                runningTimeFormatted,
                ticketPriceCalculator.calculateTicketPrice(s));
            System.out.println(showingFormatted);
        });
        System.out.println(TABLE_BORDER);
        System.out.println();
        List<Schedule> schedules = getSchedules();

        try {
            String schedulesSerialized = OBJECT_MAPPER.writeValueAsString(schedules);
            System.out.println(schedulesSerialized);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public List<Schedule> getSchedules() {
        return schedule.stream()
            .map(s -> new Schedule(
                s.getSequenceOfTheDay(),
                s.getStartTime().toString(),
                s.getMovie().getTitle(),
                humanReadableFormat(s.getMovie().getRunningTime()),
                ticketPriceCalculator.calculateTicketPrice(s) + ""))
            .collect(Collectors.toList());
    }

    public String humanReadableFormat(Duration duration) {
        long hour = duration.toHours();
        long remainingMin = duration.toMinutes() - TimeUnit.HOURS.toMinutes(duration.toHours());

        return String.format("(%s hour%s %s minute%s)", hour, handlePlural(hour), remainingMin, handlePlural(remainingMin));
    }

    // (s) postfix should be added to handle plural correctly
    private String handlePlural(long value) {
        if (value == 1) {
            return "";
        }
        else {
            return "s";
        }
    }

    public static void main(String[] args) {
        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
            sequenceDiscounts,
            LocalTime.of(11, 0),
            LocalTime.of(16, 0));
        Theater theater = new Theater(LocalDateProvider::present, ticketPriceCalculator);
        theater.printSchedule();
    }
}
