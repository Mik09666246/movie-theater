package com.jpmc.theater.utils;

import com.jpmc.theater.model.Reservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationPriceCalculatorTest {
    @Mock
    private TicketPriceCalculator ticketPriceCalculator;

    private ReservationPriceCalculator reservationPriceCalculator;

    @BeforeEach
    void before() {
        reservationPriceCalculator = new ReservationPriceCalculator(ticketPriceCalculator);
    }

    @Test
    void calculateReservationPriceWith1Ticket() {
        when(ticketPriceCalculator.calculateTicketPrice(any())).thenReturn(2.0);
        double calculated = reservationPriceCalculator.calculateReservationPrice(new Reservation(null, null, 1));
        assertEquals(2, calculated);
    }

    @Test
    void calculateReservationPriceWith2Tickets() {
        when(ticketPriceCalculator.calculateTicketPrice(any())).thenReturn(2.0);
        double calculated = reservationPriceCalculator.calculateReservationPrice(new Reservation(null, null, 2));
        assertEquals(4, calculated);
    }
}