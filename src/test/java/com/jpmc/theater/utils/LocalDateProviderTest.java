package com.jpmc.theater.utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class LocalDateProviderTest {
    @Test
    void present() {
        assertEquals(LocalDate.now(), LocalDateProvider.present());
    }
}