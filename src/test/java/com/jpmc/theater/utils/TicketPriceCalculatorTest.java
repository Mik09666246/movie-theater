package com.jpmc.theater.utils;

import com.jpmc.theater.model.Movie;
import com.jpmc.theater.model.Showing;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TicketPriceCalculatorTest {
    @Test
    void calculateTicketPrice1st() {
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
        Showing showing = new Showing(turningRed, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
                sequenceDiscounts,
                LocalTime.of(11, 0),
                LocalTime.of(16, 0));
        assertEquals(8.0, ticketPriceCalculator.calculateTicketPrice(showing));
    }

    @Test
    void calculateTicketPrice2nd() {
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
        Showing showing = new Showing(turningRed, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
                sequenceDiscounts,
                LocalTime.of(11, 0),
                LocalTime.of(16, 0));
        assertEquals(9.0, ticketPriceCalculator.calculateTicketPrice(showing));
    }

    @Test
    void calculateTicketPriceSpecial() {
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 1);
        Showing showing = new Showing(turningRed, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
                sequenceDiscounts,
                LocalTime.of(11, 0),
                LocalTime.of(16, 0));
        assertEquals(8.8, ticketPriceCalculator.calculateTicketPrice(showing));
    }

    @Test
    void calculateTicketPriceMidDay() {
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 1);
        Showing showing = new Showing(turningRed, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 0)));

        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
                sequenceDiscounts,
                LocalTime.of(11, 0),
                LocalTime.of(16, 0));
        assertEquals(8.25, ticketPriceCalculator.calculateTicketPrice(showing));
    }
}