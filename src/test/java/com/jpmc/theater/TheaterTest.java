package com.jpmc.theater;

import com.jpmc.theater.utils.LocalDateProvider;
import com.jpmc.theater.utils.TicketPriceCalculator;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TheaterTest {
    @Test
    void getSchedules() {
        Map<Integer, Integer> sequenceDiscounts = Map.of(1, 3, 2, 2, 7, 1);
        TicketPriceCalculator ticketPriceCalculator = new TicketPriceCalculator(1,
                sequenceDiscounts,
                LocalTime.of(11, 0),
                LocalTime.of(16, 0));
        Theater theater = new Theater(LocalDateProvider::present, ticketPriceCalculator);
        assertDoesNotThrow(theater::printSchedule);
    }
}